﻿//Kevin Hellmuth (KevinsQuest87@gmail.com)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The boxer's innerSphere handles all proximity varialbes for both close combat and ALL ring Dynamics
//This script is called from the boxer_RingDynamics_StateMachine

public class boxer_InnerSphere : MonoBehaviour {

    //This is simply for testing feedback 
    public GameObject followLight;
  

    // Use this for initialization
    void Start () {
        followLight.gameObject.GetComponent<Light>().color = Color.white;

    }
	


    public void inCornerTrue() {

        Debug.Log("InCorner");
     
        followLight.gameObject.GetComponent<Light>().color = Color.red;
    }

   

    public void OnRailTrack() {

        followLight.gameObject.GetComponent<Light>().color = Color.magenta;
   
        Debug.Log("OnRails");
    }

    public void InCenterRing() {

        followLight.gameObject.GetComponent<Light>().color = Color.white;
        Debug.Log("exitCorner");
      
    }

    public void InOuterSphere() {

        followLight.gameObject.GetComponent<Light>().color = Color.green;

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag=="cornerTrig")
        {
            inCornerTrue();
        }

        if (other.tag=="railTrig")
        {
            OnRailTrack();
        }

        if (other.tag=="outerInflu_Red")
        {
            InOuterSphere();
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "cornerTrig")
        {
            InCenterRing();
        }

        if (other.tag == "railTrig")
        {
            InCenterRing();
        }

        if (other.tag=="outerInflu_Red")
        {
            InCenterRing();

        }
    }

    public void overTaken() {

      
        Debug.Log("overTaken");

        followLight.gameObject.GetComponent<Light>().color = Color.cyan;
    }

    public void escapedRadius() {
        followLight.gameObject.GetComponent<Light>().color = Color.white;
       
        Debug.Log("escapedRadius");

    }



}
