﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Using raycasts to find the angle that the boxer is at in comparison to the corner in order to separate properly.
//                Start & End dates 06/06/2017 - 06/10/2017
//                References:
//                        Links:
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerPostRaycast : MonoBehaviour {

    [SerializeField]
    [Tooltip("Which boxer is this script looking for?")]
    string boxerToLookFor, boxerToLookFor2;
    GameObject boxer1, boxer2; // The boxer that is found by the name you declare in the editor

    [SerializeField]
    [Tooltip("Cornerposts that are perpendicular to the post this script is attached to.")]
    GameObject cornerPost1, cornerPost2, cornerPost3;

    RaycastHit hit;
    RaycastHit hit2;

    public float dist1;
    public float dist2;

    [SerializeField]
    [Tooltip("What range does the ray begin firing at the player?")]
    float range = 2;

    float angle;
    float angle2;

    Vector3 direction1 = Vector3.zero;
    Vector3 direction2 = Vector3.zero;

    bool hittingBoxer1, hittingBoxer2 = false;

    public bool useThis = false;
	// Use this for initialization
	void Start () {
        boxer1 = GameObject.Find(boxerToLookFor);
        boxer2 = GameObject.Find(boxerToLookFor2);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        dist1 = Vector3.Distance(transform.position, boxer1.transform.position);
        direction1 = boxer1.transform.position - transform.position;
        if(Vector3.Distance(transform.position, boxer1.transform.position) < range)
        {
            Debug.DrawRay(transform.position, direction1);
            RayCastBoxer1();
        }

        if (dist1 < range && dist2 < range && Input.GetKeyDown(KeyCode.E))
        {
            MoveBoxer();
        }
        if(dist1 <= range || dist2 <= range)
        {
            useThis = true;
        }
        else
        {
            useThis = false;
        }
        dist2 = Vector3.Distance(transform.position, boxer2.transform.position);
        direction2 = boxer2.transform.position - transform.position;
        if(Vector3.Distance(transform.position, boxer2.transform.position) < range)
        {
            Debug.DrawRay(transform.position, direction2, Color.red);
            RayCastBoxer2();

        }
	}
    public void MoveBoxer()
    {
        if(hittingBoxer1 == true && hittingBoxer2 == true)
        {
            if(angle < 43 && angle2 > 46.8)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost1.transform.position, 1.0f);
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost2.transform.position, 1.0f);
            }
            if(angle > 46.8 && angle2 < 43)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost2.transform.position, 1.0f);
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost1.transform.position, 1.0f);
            }
            if(angle < 43 && angle2 <= 46.8 && angle2 >= 43)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if(angle > 46.8 && angle2 <= 46.8 && angle2 >= 43)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost2.transform.position, 2.0f);
            }
            if (angle2 < 43 && angle <= 46.8 && angle >= 43)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if (angle2 > 46.8 && angle <= 46.8 && angle >= 43)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost2.transform.position, 2.0f);
            }

        }
        if(hittingBoxer1 == true && hittingBoxer2 == true && dist1 > dist2)
        {
            if (angle < 43 && angle2 < 43)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if (angle > 46.8 && angle2 > 46.8)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost2.transform.position, 2.0f);
            }

        }
        if (hittingBoxer1 == true && hittingBoxer2 == true && dist1 < dist2)
        {
            if (angle < 43 && angle2 < 43)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if (angle > 46.8 && angle2 > 46.8)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost2.transform.position, 2.0f);
            }
        }
        if(hittingBoxer1 == true && hittingBoxer2 == false && dist2 > dist1)
        {
            if(angle < 43)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if(angle > 46.8)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost2.transform.position, 2.0f);
            }
            if (angle >= 43 && angle <= 46.8)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost3.transform.position, 1.0f);
            }
        }
        if(hittingBoxer2 == true && hittingBoxer1 == false && dist1 > dist2)
        {
            if(angle2 < 43)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost1.transform.position, 2.0f);
            }
            if(angle2 > 46.8)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost2.transform.position, 2.0f);
            }
            if (angle2 >= 43 && angle2 <= 46.8)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerPost3.transform.position, 1.0f);
            }
        }
    }
    void RayCastBoxer1()
    {
        if (Physics.Raycast(transform.position, direction1, out hit, 5.0f))
        {
            if (hit.collider.name == boxerToLookFor)
            {
                angle = Vector3.Angle(hit.point, Vector3.right);

                //Debug.Log("I'm hitting the Boxer");
                hittingBoxer1 = true;
                //Debug.Log("Angle of " + angle);
            }
            else
            {
                //Debug.Log("I'm not hitting the boxer");
                hittingBoxer1 = false;
            }
        }
    }

    void RayCastBoxer2()
    {
        if (Physics.Raycast(transform.position, direction2, out hit2, dist2) && hit2.collider == boxer2)
        {
                angle2 = Vector3.Angle(hit.point, Vector3.right);
                //Debug.Log("I'm hitting the Boxer");
                Debug.Log("Angle of " + angle2);
                hittingBoxer2 = true;
            }
            else
            {
                //Debug.Log("I'm not hitting the boxer");
                hittingBoxer2 = false;
            }
        }
    }



