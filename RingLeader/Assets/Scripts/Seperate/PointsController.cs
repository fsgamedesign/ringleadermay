﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Current placeholder for points system used to decrement and increment boxer points.
//                Start & End dates 06/01/2017
//                References:
//                        Links:
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsController : MonoBehaviour {
    //How many points the player starts the round with
    int player1Points = 10;
    int player2Points = 10;

    //How many warnings the player start the round with
    int player1Warnings = 0;
    int player2Warnings = 0;
    
    public int GetPlayer1Points()
    {
        return player1Points;
    }
    public int GetPlayer2Points()
    {
        return player2Points;
    }

    public void ModPlayer1Points(int _value)
    {
        player1Points += _value;
        if(player1Points <= 0)
        {
            player1Points = 0;
        }
    }
    public void ModPlayer2Points(int _value)
    {
        player2Points += _value;
        if(player2Points <= 0)
        {
            player2Points = 0;
        }
    }
    public void ModPlayer1Warnings(int _value)
    {
        player1Warnings += _value;
    }
    public void ModPlayer2Warnings(int _value)
    {
        player2Warnings += _value;
    }
}
