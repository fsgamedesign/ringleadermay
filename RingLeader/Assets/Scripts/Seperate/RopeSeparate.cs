﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Find position of player when near ropes in order to separate properly.
//              This feature can be used by pressing E while the player is on/near the ropes and is in clinch range of the other boxer.
//                Start & End dates 06/10/2017 - 06/11/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeSeparate : MonoBehaviour {

    [SerializeField]
    GameObject cornerPost, cornerPost2;

    [SerializeField]
    string boxerToCheckFor, boxerToCheckFor2;
    GameObject boxer, boxer2;
    [SerializeField]
    CornerSeparate corner1, corner2;
    bool boxerHere, boxerHere2 = false;
    public bool useThis = false;

	// Use this for initialization
	void Start () {
        

        boxer = GameObject.Find(boxerToCheckFor);
        boxer2 = GameObject.Find(boxerToCheckFor2);
	}

    void OnEnable()
    {
        SeperateBoxers.SeperateTheBoxers += MoveBoxers;
    }

    void OnDisable()
    {
        SeperateBoxers.SeperateTheBoxers -= MoveBoxers;
    }

    // Update is called once per frame
    void Update () {

        if (boxerHere == false && boxerHere2 == false)
            useThis = false;
	}

    void OnTriggerStay(Collider other)
    {
        if(other.name == boxerToCheckFor)
        {
            boxerHere = true;
            useThis = true;
        }
        if(other.name == boxerToCheckFor2)
        {
            boxerHere2 = true;
            useThis = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.name == boxerToCheckFor)
        {
            boxerHere = false;
        }
        if (other.name == boxerToCheckFor2)
        {
            boxerHere2 = false;
        }
    }
    void MoveBoxers()
    {
        if (corner1.useThis == false && corner2.useThis == false)
        {
            float dist = Vector3.Distance(boxer.transform.position, boxer2.transform.position);
            if (dist < 3)
            {
                if (corner1.useThis == false && corner2.useThis == false)
                {
                    if (boxerHere == true && boxerHere2 == false)
                    {
                        boxer2.transform.position += boxer2.transform.position - boxer.transform.position;
                    }
                    else if (boxerHere == false && boxerHere2 == true)
                    {
                        boxer.transform.position += boxer.transform.position - boxer2.transform.position;
                    }
                    else if (boxerHere == true && boxerHere2 == true)
                    {
                        if (corner1.dist1 < corner1.dist2)
                        {
                            boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost2.transform.position, dist / 2);
                            boxer.transform.position = Vector3.MoveTowards(boxer.transform.position, cornerPost.transform.position, dist / 2);
                        }
                        else if (corner1.dist1 > corner1.dist2)
                        {
                            boxer.transform.position = Vector3.MoveTowards(boxer.transform.position, cornerPost2.transform.position, dist / 2);
                            boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerPost.transform.position, dist / 2);
                        }
                    }
                }
            }
        }
        else
        {
            return;
        }
    }
            
}
