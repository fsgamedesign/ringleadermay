﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Checking where the player is inside of the corner and moving them accordingly.
//              This feature can be used by pressing E while at least one boxer is in the corner, and in clinch range.
//                Start & End dates 06/13/2017
//                References: 
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerSeparate : MonoBehaviour {

    public float dist1;
    public float dist2;
    [SerializeField]
    [Tooltip("Which boxer is this script looking for?")]
    string boxerToLookFor, boxerToLookFor2;
    GameObject boxer1, boxer2; // The boxer that is found by the name you declare in the editor
    [SerializeField]
    PlayerCheck checkRight, checkCorner, checkLeft;

    [SerializeField]
    GameObject cornerRight, cornerDiagonal, cornerLeft;
    public bool useThis;

    // Use this for initialization
    void Start () {
        boxer1 = GameObject.Find(boxerToLookFor);
        boxer2 = GameObject.Find(boxerToLookFor2);
    }

    void OnEnable()
    {
        SeperateBoxers.SeperateTheBoxers += MoveBoxers;
    }

    void OnDisable()
    {
        SeperateBoxers.SeperateTheBoxers -= MoveBoxers;
    }

    // Update is called once per frame
    void Update() {
        dist1 = Vector3.Distance(transform.position, boxer1.transform.position);
        dist2 = Vector3.Distance(transform.position, boxer2.transform.position);

        if (checkRight.boxer1Here == true || checkRight.boxer2Here == true || checkLeft.boxer1Here == true || checkLeft.boxer2Here == true || checkCorner.boxer1Here == true || checkCorner.boxer2Here)
        {
            useThis = true;
        }
        else
        {
            useThis = false;
        }
    }

    void MoveBoxers()
    {
        float dist = Vector3.Distance(boxer1.transform.position, boxer2.transform.position);
        if (dist < 3)
        {
            if (checkLeft.boxer1Here == true && checkRight.boxer1Here == false && checkLeft.boxer2Here == false && checkRight.boxer2Here == false)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerLeft.transform.position, 1);
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerRight.transform.position, 1);
            }
            if (checkCorner.boxer1Here == true && dist1 < dist2)
            {
                if (checkRight.boxer2Here == true && checkLeft.boxer2Here == false)
                {
                    boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerRight.transform.position, 2);
                }
                else if (checkLeft.boxer2Here == true && checkRight.boxer2Here == false)
                {
                    boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerLeft.transform.position, 2);
                }
                else if (checkCorner.boxer2Here == true)
                {
                    boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerDiagonal.transform.position, 2);
                }
                else if (dist2 < 3 && checkCorner.boxer2Here == false && checkRight.boxer2Here == false && checkLeft.boxer2Here == false)
                {
                    boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerDiagonal.transform.position, 2);
                }
            }
            if (checkCorner.boxer2Here == true && dist2 < dist1)
            {
                if (checkRight.boxer1Here == true && checkLeft.boxer1Here == false)
                {
                    boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerRight.transform.position, 2);
                }
                else if (checkLeft.boxer1Here == true && checkRight.boxer1Here == false)
                {
                    boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerLeft.transform.position, 2);
                }
                else if (checkCorner.boxer1Here == true)
                {
                    boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerDiagonal.transform.position, 2);
                }
                else if (dist1 < 3 && checkCorner.boxer1Here == false && checkRight.boxer1Here == false && checkLeft.boxer1Here == false)
                {
                    boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerDiagonal.transform.position, 2);
                }
            }
            if (checkRight.boxer1Here == true && checkRight.boxer2Here == false && checkLeft.boxer2Here == false && checkCorner.boxer2Here == false ||
                checkRight.boxer1Here == true && dist1 < dist2 && checkRight.boxer2Here == true && checkLeft.boxer2Here == false && checkCorner.boxer2Here == false)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerRight.transform.position, 2);
            }
            if (checkLeft.boxer1Here == true && checkRight.boxer2Here == false && checkLeft.boxer2Here == false && checkCorner.boxer2Here == false ||
                checkLeft.boxer1Here == true && dist1 < dist2 && checkRight.boxer2Here == false && checkLeft.boxer2Here == true && checkCorner.boxer2Here == false)
            {
                boxer2.transform.position = Vector3.MoveTowards(boxer2.transform.position, cornerRight.transform.position, 2);
            }
            if (checkRight.boxer2Here == true && checkRight.boxer1Here == false && checkLeft.boxer1Here == false && checkCorner.boxer1Here == false ||
                checkRight.boxer2Here == true && dist2 < dist1 && checkRight.boxer1Here == true && checkLeft.boxer1Here == false && checkCorner.boxer1Here == false)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerRight.transform.position, 2);
            }
            if (checkLeft.boxer2Here == true && checkRight.boxer1Here == false && checkLeft.boxer1Here == false && checkCorner.boxer1Here == false ||
                checkLeft.boxer2Here == false && dist2 < dist1 && checkRight.boxer1Here == false && checkLeft.boxer1Here == true && checkCorner.boxer1Here == false)
            {
                boxer1.transform.position = Vector3.MoveTowards(boxer1.transform.position, cornerLeft.transform.position, 2);
            }
        }
        
    }
}
