﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateDebugText : MonoBehaviour {

    [SerializeField]
    GameStateManager gameStateManager;

    [SerializeField]
    Text currentGameStateText;
	
	// Update is called once per frame
	void Update () {
        currentGameStateText.text = gameStateManager.GetCurrentGameState().ToString();
	}
}
