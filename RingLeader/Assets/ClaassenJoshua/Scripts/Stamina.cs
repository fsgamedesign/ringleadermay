﻿/*        
//        Developer Name: Joshua Claassen
//         Contribution: Basic system for the boxers stamina and having the punches all take away stamina. boxers have a rest time for when they run negative on their stamina.
//                Feature - Stamina for punching.
//                Start & End dates 6/14/2017 - 6/15/2017
//                References:
//                        Links:
//*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stamina : MonoBehaviour {

#region Variables
    [SerializeField]
    int maxStamina = 10;

    private int curStamina;

    [SerializeField]
    float staminaTimer = 5.0f;

    [SerializeField]
    [Tooltip("rest time when out of stamina")]
    float restTimer = 5.0f;

    [SerializeField]
    [Tooltip("Amount of stamina it takes for the punch")]
    int jabCost = 1, crossCost = 2, hookCost = 2, uppercutCost = 3;

    private bool startStamTime;

    private float maxStamTimer = 5.0f;


#endregion  
    
    // Use this for initialization
    void Start () {
        curStamina = maxStamina;
        maxStamTimer = staminaTimer;
	}
	
	// Update is called once per frame
	void Update () {
        if (startStamTime == true)
        {
            staminaTimer -= Time.deltaTime;
        }

        if (staminaTimer <= 0)
        {
            curStamina = maxStamina;
            startStamTime = false;
            staminaTimer = maxStamTimer;
        }

        NegativeStamina();
	}

    public void UseJab()
    {
        curStamina -= jabCost;
        StartStamTimer();
        StaminaDebugLog();
    }

    public void UseCross()
    {
        curStamina -= crossCost;
        StartStamTimer();
        StaminaDebugLog();
    }

    public void UseHook()
    {
        curStamina -= hookCost;
        StartStamTimer();
        StaminaDebugLog();
    }

    public void UseUppercut()
    {
        curStamina -= uppercutCost;
        StartStamTimer();
        StaminaDebugLog();
    }

    public void UseAllin()
    {
        curStamina -= maxStamina;
        StartStamTimer();
        StaminaDebugLog();
    }

    public int GetCurStamina()
    {
        return curStamina;
    }

    public int GetMaxStamina()
    {
        return maxStamina;
    }

    private void StartStamTimer()
    {
        startStamTime = true;
    }

    private void StaminaDebugLog()
    {
        Debug.Log(gameObject.name + " has " + curStamina + " Stamina Left");
    }

    private void NegativeStamina()
    {
        if (curStamina < 0)
        {
            staminaTimer = restTimer;
        }
    }
}
