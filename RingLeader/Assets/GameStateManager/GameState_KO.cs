﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/15/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState_KO : GameState {

    //references/parameters/variables needed by this gamestate


    public override void InitializeState()
    {
        Debug.Log(string.Format("{0} is initializing", GetType().ToString()));

        //TODO: anything this game state needs to do on awake at the beginning of application runtime.

        // if the initializtion completes with no null references or incomplete tasks
        // set the current status to staged. Otherwise set it to failed.
        currentStatus = StateStatus.Staged;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void StartState()
    {
        currentStatus = StateStatus.Start;

        //TODO: anything this game state needs to do first when taking over as the current game state.

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));
    }

    public override void UpdateState()
    {
        //TODO: anything this game state needs to do every frame.
    }

    public override void ExitState()
    {
        currentStatus = StateStatus.Exit;

        Debug.Log(string.Format("{0} status: {1}", GetType().ToString(), currentStatus.ToString()));

        currentStatus = StateStatus.Staged;
    }

    //functions/methods needed by this gamestate
}
