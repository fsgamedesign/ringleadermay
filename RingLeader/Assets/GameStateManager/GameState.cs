﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/16/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc This is the contract class for any game states added to the game state manager.

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameState : MonoBehaviour {

    public Constants.GameState gameState; // This will be how external classes will know what state is active.

    public List<Constants.GameState> availableGameStateTransitions = new List<Constants.GameState>(); // The states that can be switched to from this state.

    /* StateStatus
    Initializing is the first state a game state is in. 
    Staged is when the state is active but a transition is being validated or if the state is waiting to be transitioned to.
    Failed is when the state failed its start functionality.
    Start is when a state is first transitioned to. 
    Update is the frame by frame functionality of a state
    Exit is when a state is being transitioned away from.
    */
    [HideInInspector]
    public enum StateStatus { Initializing, Staged, Failed, Start, Update, Exit }

    public StateStatus currentStatus = StateStatus.Initializing;

    // THIS HAPPENS IN SCENE 0 IN THE BUILD ORDER
    public abstract void InitializeState(); // Anything the state needs to do at awake before the first frame of the game runtime.

    public abstract void StartState(); // anything the state needs to do when it is transitioned to from another state

    public abstract void UpdateState(); // anything the state needs to do on every frame while it is active

    public abstract void ExitState(); // anything the state needs to do when it is transitioning to another state
}
