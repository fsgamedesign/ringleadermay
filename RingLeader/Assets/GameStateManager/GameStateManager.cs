﻿/*------------------------------------------------------------------------------------------------
 Original Author: John Hartzell
 Date: 5/16/2017
 Credit (online ref./additional authors): 

 Purpose: https://trello.com/c/i6uxSvXc This is the main app controller. It implements and presides over the flow of the
          game state and gives access to individual game states. It is a singleton https://en.wikipedia.org/wiki/Singleton_pattern,
          meaning there should only be one of this object over the lifetime of the runtime. It is created at the beginning of the first scene and destroys
          when the application quits. 

*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// require the game states as components
[RequireComponent(typeof(GameState_Menus))]
[RequireComponent(typeof(GameState_BoxerSelection))]
[RequireComponent(typeof(GameState_BoutConditions))]
[RequireComponent(typeof(GameState_CornerState))]
[RequireComponent(typeof(GameState_RoundAction))]
[RequireComponent(typeof(GameState_StandingCount))]
[RequireComponent(typeof(GameState_RoundPause))]
[RequireComponent(typeof(GameState_StandingCount))]
[RequireComponent(typeof(GameState_TKO))]
[RequireComponent(typeof(GameState_KO))]
[RequireComponent(typeof(GameState_JudgeCards))]
[RequireComponent(typeof(GameState_GameOver))]

public class GameStateManager : MonoBehaviour {

    //need a static instance of this class
    public static GameStateManager gameStateManager { get; private set; }

    [Tooltip("This field needs to be set, in the inspector, to the first state the game should be in. Drag in from the avialable game state components attached to this object.")]
    [SerializeField]
    private GameState currentGameState;

    [SerializeField]
    private string sCurrentGameState;

    // list of game states: all game states should be in.
    private List<GameState> gameStates = new List<GameState>();

    // all game states need an accessible reference.
    public GameState_Menus menusGameState { get; private set; }
    public GameState_BoxerSelection boxerSelectionGameState { get; private set; }
    public GameState_BoutConditions boutConditionsGameState { get; private set; }
    public GameState_CornerState cornerStateGameState { get; private set; }
    public GameState_RoundAction roundActionGameState { get; private set; }
    public GameState_StandingCount standingCountGameState { get; private set; }
    public GameState_RoundPause roundPauseGameState { get; private set; }
    public GameState_TKO tkoGameState { get; private set; }
    public GameState_KO koGameState { get; private set; }
    public GameState_JudgeCards judgeCardsGameState { get; private set; }
    public GameState_GameOver gameOverGameState { get; private set; }

    public delegate void GameStateChanged(Constants.GameState newState);
    public static event GameStateChanged GameStateHasChanged;

    private void Awake()
    {
        // enforce singleton pattern http://answers.unity3d.com/comments/577357/view.html
        if (gameStateManager == null)
        {
            Debug.Log("Creating game state manager instance.");
            gameStateManager = this;
            DontDestroyOnLoad(gameStateManager);

        }else
        {
            Debug.Log(string.Format("Enforcing singleton pattern. Removing {0} attached to {1}",GetType().ToString(), gameObject.name));
            Destroy(this);
        }

        Debug.Log("Begin game state initialization...");

        // get game states
        menusGameState = GetComponent<GameState_Menus>();
        boxerSelectionGameState = GetComponent<GameState_BoxerSelection>();
        boutConditionsGameState = GetComponent<GameState_BoutConditions>();
        cornerStateGameState = GetComponent<GameState_CornerState>();
        roundActionGameState = GetComponent<GameState_RoundAction>();
        standingCountGameState = GetComponent<GameState_StandingCount>();
        roundPauseGameState = GetComponent<GameState_RoundPause>();
        tkoGameState = GetComponent<GameState_TKO>();
        koGameState = GetComponent<GameState_KO>();
        judgeCardsGameState = GetComponent<GameState_JudgeCards>();
        gameOverGameState = GetComponent<GameState_GameOver>();

        // add game states to list
        gameStates.Add(menusGameState);
        gameStates.Add(boxerSelectionGameState);
        gameStates.Add(boutConditionsGameState);
        gameStates.Add(cornerStateGameState);
        gameStates.Add(roundActionGameState);
        gameStates.Add(standingCountGameState);
        gameStates.Add(roundPauseGameState);
        gameStates.Add(tkoGameState);
        gameStates.Add(koGameState);
        gameStates.Add(judgeCardsGameState);
        gameStates.Add(gameOverGameState);

        // initialize game states
        for (int i = 0; i < gameStates.Count; i++)
        {
            gameStates[i].InitializeState();
        }

        Debug.Log("Game state initialization complete.");

        if(currentGameState != null)
        {
            Debug.Log(string.Format("Starting initial state: {0}", currentGameState.GetType().ToString()));

            currentGameState.StartState();

            currentGameState.currentStatus = GameState.StateStatus.Update;

            Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));
        }
        else
        {
            Debug.Log("Initial state not set in inspector. Setting current state to menus.");

            currentGameState = menusGameState;

            currentGameState.StartState();

            currentGameState.currentStatus = GameState.StateStatus.Update;

            Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));
        }
        
    }

    // Update is called once per frame
    private void Update () {

        sCurrentGameState = currentGameState.gameState.ToString();

        // current state update function
        // this does not fire during state transition
        if(currentGameState.currentStatus == GameState.StateStatus.Update)
        {
            currentGameState.UpdateState();
        }

	}

    // TODO: Create a slow update enumerator to allow continuous game processes that dont need to happen every frame stay out of update.

    public void ChangeGameState(Constants.GameState newState)
    {
        Debug.Log(string.Format("Game state change requested. Requested State {0}", newState.ToString()));

        currentGameState.currentStatus = GameState.StateStatus.Staged; // stage the current state so that update does not unintentionally fire.

        Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));

        bool isRequestValid = ValidateChangeStateRequest(newState); // is the requested state valid in the current state's context?

        if (isRequestValid) // if yes
        {

            Debug.Log("Game state change approved.");

            GameState tempState = GetNewState(newState); // get the new state
            if (tempState != null) // does it exist?
            {
                currentGameState.ExitState(); // run the exit code of the current state

                currentGameState = tempState; // swap to the new state

                if(GameStateHasChanged != null)
                {
                    GameStateHasChanged(newState);
                }

                currentGameState.StartState(); // run the start code of the new state

                currentGameState.currentStatus = GameState.StateStatus.Update; // turn on update for the new state

                Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));
            }
            else // requested state has not been added to the game state manager, attached to the game object, or has simply not been implemented.
            {
                Debug.Log("The game state requested could not be found. Please check the game state manager.");

                currentGameState.currentStatus = GameState.StateStatus.Update; // the state transition failed, continue the current state.
            }


        }else // if no
        {
            Debug.Log(string.Format("Game state change rejected. The game state {0} is not a valid transition during {1}", newState.ToString(), currentGameState.gameState.ToString()));

            currentGameState.currentStatus = GameState.StateStatus.Update; // the state transition failed, continue the current state.
        }

    }

    public void ChangeGameState(int i)
    {

        Constants.GameState newState = (Constants.GameState)i;

        Debug.Log(string.Format("Game state change requested. Requested State {0}", newState.ToString()));

        currentGameState.currentStatus = GameState.StateStatus.Staged; // state the current state so that update does not unintentionally fire.

        Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));

        bool isRequestValid = ValidateChangeStateRequest(newState); // is the requested state valid in the current state's context?

        if (isRequestValid) // if yes
        {

            Debug.Log("Game state change approved.");

            GameState tempState = GetNewState(newState); // get the new state
            if (tempState != null) // does it exist?
            {
                currentGameState.ExitState(); // run the exit code of the current state

                currentGameState = tempState; // swap to the new state

                if (GameStateHasChanged != null)
                {
                    GameStateHasChanged(newState);
                }

                currentGameState.StartState(); // run the start code of the new state

                currentGameState.currentStatus = GameState.StateStatus.Update; // turn on update for the new state

                Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));
            }
            else // requested state has not been added to the game state manager, attached to the game object, or has simply not been implemented.
            {
                Debug.Log("The game state requested could not be found. Please check the game state manager.");

                currentGameState.currentStatus = GameState.StateStatus.Update; // the state transition failed, continue the current state.

                Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));

            }


        }
        else // if no
        {
            Debug.Log(string.Format("Game state change rejected. The game state {0} is not a valid transition during {1}", newState.ToString(), currentGameState.gameState.ToString()));

            currentGameState.currentStatus = GameState.StateStatus.Update; // the state transition failed, continue the current state.

            Debug.Log(string.Format("{0} status: {1}", currentGameState.GetType().ToString(), currentGameState.currentStatus.ToString()));
        }

    }


    private GameState GetNewState(Constants.GameState newState)
    {
        GameState temp = null; 

        for(int i = 0; i < gameStates.Count; i++) // iterate through the game states
        {
            if(newState == gameStates[i].gameState) // does the requested state match an available state?
            {
                temp = gameStates[i];
            }
        }

        return temp; // return the result of our search
    }

    private bool ValidateChangeStateRequest(Constants.GameState newState)
    {
        bool stateChangeValid = false;

        // iterate through the states that the current state can transition too.
        for(int i = 0; i < currentGameState.availableGameStateTransitions.Count; i++)
        {
            if(newState == currentGameState.availableGameStateTransitions[i]) // if the requested state matches an available state return true.
            {
                stateChangeValid = true;
                break;
            }
        }

        return stateChangeValid;

    }

    // allows other classes to know what state the game is in.
    public Constants.GameState GetCurrentGameState() 
    {
        return currentGameState.gameState;
    }
}
