﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Blocking : ByTheTale.StateMachine.State, IMovement
{
    public BoxerStateMachine bSM { get { return (BoxerStateMachine)machine; } }
    
    Transform boxerTransform;
    string horizontalAxis;
    string verticalAxis;
    CharacterController characterController;
    GameObject enemyObject;

    Vector3 moveDirection = Vector3.zero;

    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        horizontalAxis = bSM.boxerData.horizontalAxis;
        verticalAxis = bSM.boxerData.verticalAxis;
        characterController = bSM.characterController;
        enemyObject = bSM.boxerData.enemyObj;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);
        HandleMovement();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void HandleMovement()
    {
        if (Input.GetKey(bSM.boxerData.button_Crouch)) {
            bSM.boxerAnimator.SetBool("isCrouching", true);
        } else {
            bSM.boxerAnimator.SetBool("isCrouching", false);
        }

        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = boxerTransform.TransformDirection(moveDirection);
        moveDirection *= bSM.boxerData.moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        boxerTransform.position = new Vector3(boxerTransform.position.x, 0, boxerTransform.position.z); //Keeps the boxer's feet on the boxing canvas.
    }
}