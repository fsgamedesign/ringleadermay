﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerData : MonoBehaviour
{
    public bool isSouthpaw;

    [SerializeField] string enemyTag;
    [HideInInspector] public GameObject enemyObj;

    public BoxerColor boxerColor;
    public enum BoxerColor
    {
        Red,
        Blue
    }

    public float moveSpeed;

    [Header("Input")]
    public string horizontalAxis;
    public string verticalAxis;
    public KeyCode button_Left; // "1" on control diagram
    public KeyCode button_Right; // "2" on control diagram
    public KeyCode button_Defend; // "3" on control diagram
    public KeyCode button_Hook; // "A/4" on control diagram
    public KeyCode button_Uppercut; // "B/5" on control diagram
    public KeyCode button_Crouch; // "C/6" on control diagram

    [HideInInspector] public bool isPunching;
    [HideInInspector] public bool isDamageEnabled_LeftGlove;
    [HideInInspector] public bool isDamageEnabled_RightGlove;
    [HideInInspector] public bool isCrouching;


    void Awake()
    {
        AssignEnemy();
    }

    void AssignEnemy()
    {
        if (boxerColor == BoxerColor.Red)
        {
            enemyObj = GameObject.FindGameObjectWithTag("BlueBoxer");
        }
        else
        {
            enemyObj = GameObject.FindGameObjectWithTag("RedBoxer");
        }
    }
}