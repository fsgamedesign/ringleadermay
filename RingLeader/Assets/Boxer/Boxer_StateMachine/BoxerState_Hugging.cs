﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_Hugging : ByTheTale.StateMachine.State
{
    public BoxerStateMachine bSM { get { return (BoxerStateMachine)machine; } }

    public override void Initialize()
    {
        base.Initialize();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
    }

    public override void Exit()
    {
        base.Exit();
    }
}