﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxerData))]
[RequireComponent(typeof(CharacterController))]


public class BoxerStateMachine : ByTheTale.StateMachine.MachineBehaviour
{
    [HideInInspector] public Animator boxerAnimator;
    [HideInInspector] public BoxerData boxerData;
    [HideInInspector] public CharacterController characterController;

    public override void AddStates()
    {
        AddState<BoxerState_MovePunch>();
        AddState<BoxerState_Blocking>();
        AddState<BoxerState_Commanded>();
        AddState<BoxerState_Dodging>();
        AddState<BoxerState_Hugging>();
        AddState<BoxerState_KnockedDown>();
        AddState<BoxerState_Paused>();

        SetInitialState<BoxerState_MovePunch>();
    }

    public void Awake()
    {
        boxerAnimator = GetComponent<Animator>();
        boxerData = GetComponent<BoxerData>();
        characterController = GetComponent<CharacterController>();


    }

    public override void LateUpdate()
    {
        base.LateUpdate();
    }

    #region Animation Event Methods
    public void ToggleIsPunching(int i)
    {
        if (i == 0) {
            boxerData.isPunching = false;
        } else {
            boxerData.isPunching = true;
        }
    }

    public void RightGlove_Damage_Enable()
    {
        boxerData.isDamageEnabled_RightGlove = true;
	
    }
    public void RightGlove_Damage_Disable()
    {
        boxerData.isDamageEnabled_RightGlove = false;
    }

    public void LeftGlove_Damage_Enable()
    {
        boxerData.isDamageEnabled_LeftGlove = true;
    }
    public void LeftGlove_Damage_Disable()
    {
        boxerData.isDamageEnabled_LeftGlove = false;
    }

    public void ClearPunchCode()
    {
        boxerAnimator.SetInteger("PunchCode", 0);
    }
    #endregion
}