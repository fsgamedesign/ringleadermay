﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxerState_MovePunch : ByTheTale.StateMachine.State, IMovement, IPunching
{
    public BoxerStateMachine bSM { get { return (BoxerStateMachine)machine; } }

    Transform boxerTransform;
    string horizontalAxis;
    string verticalAxis;
    CharacterController characterController;
    GameObject enemyObject;

    Vector3 moveDirection = Vector3.zero;

    int punchCode = 0;

    public override void Initialize()
    {
        base.Initialize();
        boxerTransform = bSM.boxerData.transform;
        horizontalAxis = bSM.boxerData.horizontalAxis;
        verticalAxis = bSM.boxerData.verticalAxis;
        characterController = bSM.characterController;
        enemyObject = bSM.boxerData.enemyObj;
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Execute()
    {
        base.Execute();
        boxerTransform.LookAt(enemyObject.transform);
        HandleMovement();
        HandlePunching();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public void HandleMovement()
    {
        if (Input.GetKey(bSM.boxerData.button_Crouch)) {
            bSM.boxerAnimator.SetBool("isCrouching", true);
        } else {
            bSM.boxerAnimator.SetBool("isCrouching", false);
        }

        moveDirection = new Vector3(Input.GetAxis(horizontalAxis), 0, Input.GetAxis(verticalAxis));
        moveDirection = boxerTransform.TransformDirection(moveDirection);
        moveDirection *= bSM.boxerData.moveSpeed;
        characterController.Move(moveDirection * Time.deltaTime);
        boxerTransform.position = new Vector3(boxerTransform.position.x, 0, boxerTransform.position.z); //Keeps the boxer's feet on the boxing canvas.
    }

    public void HandlePunching()
    {
        if (bSM.boxerAnimator.GetBool("isCrouching") == true)
        {
            if (Input.GetKey(bSM.boxerData.button_Hook))
            {
                if (Input.GetKeyDown(bSM.boxerData.button_Left)) {
                    punchCode = 641;
                } else if (Input.GetKeyDown(bSM.boxerData.button_Right)) {
                    punchCode = 642;
                }
            }
            else if (Input.GetKey(bSM.boxerData.button_Uppercut))
            {
                if (Input.GetKeyDown(bSM.boxerData.button_Left)) {
                    punchCode = 651;
                } else if (Input.GetKeyDown(bSM.boxerData.button_Right)) {
                    punchCode = 652;
                }
            }
            else if (Input.GetKeyDown(bSM.boxerData.button_Left))
            {
                punchCode = 61;
            }
            else if (Input.GetKeyDown(bSM.boxerData.button_Right))
            {
                punchCode = 62;
            }
        }
        else
        {
            if (Input.GetKey(bSM.boxerData.button_Hook))
            {
                if (Input.GetKeyDown(bSM.boxerData.button_Left)) {
                    punchCode = 41;
                } else if (Input.GetKeyDown(bSM.boxerData.button_Right)) {
                    punchCode = 42;
                }
            }
            else if (Input.GetKey(bSM.boxerData.button_Uppercut))
            {
                if (Input.GetKeyDown(bSM.boxerData.button_Left)) {
                    punchCode = 51;
                } else if (Input.GetKeyDown(bSM.boxerData.button_Right)) {
                    punchCode = 52;
                }
            }
            else if (Input.GetKeyDown(bSM.boxerData.button_Left))
            {
                punchCode = 1;
            }
            else if (Input.GetKeyDown(bSM.boxerData.button_Right))
            {
                punchCode = 2;
            }
        }

        bSM.boxerAnimator.SetInteger("PunchCode", punchCode);
        punchCode = 0;
    }
}