﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// must implement the abstract behaviors. Mouse over the class name if you are getting errors. Chances are you haven't 
// implementd the functions in the abstract class called combo.
public class SeperateBoxers : Combo
{

    public delegate void Seperate();
    public static event Seperate SeperateTheBoxers;

    public override void DoComboResult()
    {
        if(SeperateTheBoxers != null)
        {
            SeperateTheBoxers();
        }
    }

    public override void Initilialize()
    {
        
    }
}
