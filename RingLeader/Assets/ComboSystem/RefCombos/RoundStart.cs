﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Placeholder for actual round start when rounds are formed
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundStart : Combo
{

    public override void DoComboResult()
    {
        //Call function to begin round
        Debug.Log("Round has been started!");
        //Set check to ensure player cannot start round multiple times in a single round
    }
    public override void Initilialize()
    {
        
    }
}
