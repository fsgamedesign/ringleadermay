﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution:
//                Feature - Combo system input to stop standing count
//                Start & End dates 06/06/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingCountCancel : Combo {

    StandingCountController scControl;
    StandingCountInitialization scInitialize;

	// Use this for initialization
	void Start () {
        scControl = FindObjectOfType<StandingCountController>();
        scInitialize = FindObjectOfType<StandingCountInitialization>();
	}
	
    public override void DoComboResult()
    {
        scControl.CountControl(false);
        scInitialize.enabled = true;
        enabled = false;
    }
    public override void Initilialize()
    {
     
    }
}
