﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Placeholder for stopping fights when the actual mechanism is in place
//                Start & End dates 06/13/2017
//                References:
//                        Links: https://docs.google.com/document/d/1rrxnCdJTMESW4019pNHLWB7zRl3cRWhDGoiK00d1TJE/edit?usp=sharing
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopFight : Combo {

    public override void DoComboResult()
    {
        //Call function/State to make the referee stop the fight based on fight conditions
        Debug.Log("Referee has stopped the fight");
    }
    public override void Initilialize()
    {

    }
}
