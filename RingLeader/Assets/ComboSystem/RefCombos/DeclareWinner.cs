﻿/*        
//        Developer Name: Rowan Anderson
//         Contribution: 
//                Feature - Declaring winner based on points
//                Start & End dates 06/13/2017
//                References:
//                        Links:
//*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeclareWinner : Combo {
    PointsController points;
    private void Start()
    {
        points = FindObjectOfType<PointsController>();
    }

    public override void DoComboResult()
    {
        if(points.GetPlayer1Points() > points.GetPlayer2Points())
        {
            Debug.Log("Player 1 wins!");
        }
        if(points.GetPlayer1Points() < points.GetPlayer1Points())
        {
            Debug.Log("Player 2 wins!");
        }
        if(points.GetPlayer1Points() == points.GetPlayer2Points())
        {
            Debug.Log("Draw");
        }
    }
    public override void Initilialize()
    {

    }
}
